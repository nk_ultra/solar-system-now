# Solar System NOW
A quick React and TypeScript project showing the current mean anomalies of the planets of the Solar System, with some toggle-able sliders to allow some interactivity.

https://noahkuhn.website/solarsystemnow/
