import { useState } from 'react';
import './index.css';

import { Heading } from "@ariakit/react";

interface TopDrawerProps {
    title?: string;
    subtitle?: string;
    startHidden?: boolean;
    children?: JSX.Element | JSX.Element[];
}

const TopDrawer = (props : TopDrawerProps) => {
    const [hidden, setHidden] = useState<boolean>(props.startHidden || false);
    
    return <>
        <div className={"top-drawer-container" + (hidden ? " hidden" : "")} onClick={() => setHidden(!hidden)}>
            {props.title !== undefined ? <Heading>{ props.title }</Heading> : undefined}
            <p className="small-text">{props.subtitle}</p>
            {props.children}
            {hidden ? <div className="dropdown-icon">^ wait come back</div> : undefined}
        </div>
    </>;
}

export default TopDrawer;