import { useEffect, useState } from 'react';
import { HeadingLevel } from '@ariakit/react';
import { VictoryChart, VictoryAxis, VictoryScatter, VictoryZoomContainer, StringOrNumberOrCallback, VictoryTooltip } from 'victory';
import { unit, cos, sin, pow, abs, sqrt } from 'mathjs';
import ReactSlider from 'react-slider';
import { RotateLoader } from 'react-spinners';
import { Helmet } from 'react-helmet';

import './index.css';
import TopDrawer from '../TopDrawer';

export default function Map (){
    const [loaded, setLoaded] = useState<boolean>(false);
    const [timer, setTimer] = useState<NodeJS.Timer>();
    const [slidersVisible, setSliders] = useState<boolean>(false);
    const [daysSinceJ2000, setDaysSinceJ2000] = useState<number>();
    const [dateToCheck, setCheckDate] = useState<number>();
    const [scaleFactor, setScaleFactor] = useState<number>();

    const j2000 = new Date("January 1, 2000, 11:58:56");
    useEffect(() => {
        if(daysSinceJ2000 === undefined){
            setDaysSinceJ2000(((new Date()).getTime() - j2000.getTime()) / 86400000);
            setCheckDate(daysSinceJ2000);
            setScaleFactor(1000);
            setLoaded(true);
            setTimer(setInterval((): void => {
                setDaysSinceJ2000(((new Date()).getTime() - j2000.getTime()) / 86400000);
                setCheckDate(daysSinceJ2000);
            }, 250));
        }
    }, []);

    // there are so few of these there's no need for any database
    // and, um, i doubt it'll be a very dynamic list
    const astronomicalBodyData = [
        {name: "Sun", meanAnomalyAtEpoch: 0, distance: 0, color: "white", size: 10, sweepRate: 0},
        {name: "Mercury", meanAnomalyAtEpoch: 174.796, distance: 1, color: "gray", size: 2, sweepRate: 4.09},
        {name: "Venus", meanAnomalyAtEpoch: 50.115, distance: 2, color: "palegoldenrod", size: 3.5, sweepRate: 1.60},
        {name: "Earth", meanAnomalyAtEpoch: 358.617, distance: 3, color: "lightskyblue", size: 3.5, sweepRate: 0.99},
        {name: "Mars", meanAnomalyAtEpoch: 19.412, distance: 4, color: "maroon", size: 2.5, sweepRate: 0.52},
        {name: "Jupiter", meanAnomalyAtEpoch: 20.020, distance: 5, color: "tan", size: 6, sweepRate: 0.083},
        {name: "Saturn", meanAnomalyAtEpoch: 317.020, distance: 6, color: "khaki", size: 5.5, sweepRate: 0.033},
        {name: "Uranus", meanAnomalyAtEpoch: 142.239, distance: 7, color: "powderblue", size: 4.5, sweepRate: 0.012},
        {name: "Neptune", meanAnomalyAtEpoch: 259.883, distance: 8, color: "royalblue", size: 4.5, sweepRate: 0.0060},
    ];

    const eachFill: StringOrNumberOrCallback = ({datum}) => datum.fill;

    return (
        <>
            <Helmet>
                <title>Solar System NOW</title>
                <meta name="description" content="An approximation of the current mean anomalies of the planets of the Solar System in a minimalist interactive diagram" />
                <meta name="author" content="Noah Kuhn" />
                <meta name="keywords" content="current mean anomaly" />
                <meta property="og:description" content="An approximation of the current mean anomalies of the planets of the Solar System in a minimalist interactive diagram" />
                <meta property="og:title" content="Solar System NOW" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://noahkuhn.website/solarsystemnow" />
            </Helmet>
            <HeadingLevel>
                <TopDrawer
                    title="Solar System NOW*"
                    subtitle={
                        "*Not to scale. Angular offsets from +x represent mean anomaly."
                    }
                >
                    {loaded
                        ? <p>J2000 {
                            (dateToCheck === undefined
                                ? ("+ " + (daysSinceJ2000 as number).toFixed(6)
                                    || "some undefined number of")
                                : (dateToCheck >= 0 ? "+ " : "- ") + abs(dateToCheck))
                            } Earth days</p>
                        : undefined
                    }
                </TopDrawer>
                {loaded
                    ? <>
                        {slidersVisible
                            ? <>
                                <ReactSlider
                                    className="big-vertical-slider"
                                    thumbClassName="thumb"
                                    trackClassName="track"
                                    defaultValue={daysSinceJ2000 as number}
                                    max={100 * (scaleFactor || 1)}
                                    min={-100 * (scaleFactor || 1)}
                                    pageFn={step => step * (scaleFactor || 10)}
                                    orientation="vertical"
                                    invert
                                    onChange={(value, _) => {clearInterval(timer); setCheckDate(value);}}
                                    renderThumb={(props, state) => <div {...props}><br />{(state.valueNow)}</div>}
                                />
                                <ReactSlider
                                    className="small-vertical-slider"
                                    thumbClassName="thumb"
                                    trackClassName="track"
                                    defaultValue={3}
                                    max={3}
                                    min={0}
                                    pageFn={_ => 0}
                                    orientation="vertical"
                                    invert
                                    onChange={(value, _) => {clearInterval(timer); setScaleFactor(pow(10,value) as number);}}
                                    renderThumb={(props, state) => <div {...props}><br />{"x" + pow(10,state.valueNow) as string}</div>}
                                />
                            </>
                            : undefined
                        }
                        <div className="map-container">
                            <VictoryChart
                                padding={-10}
                                height={275}
                                width={500}
                                domain={{x:[-18,18], y:[-10,10]}}
                                containerComponent={<VictoryZoomContainer />}
                            >
                                <VictoryAxis crossAxis tickValues={[-100]} />
                                <VictoryAxis dependentAxis crossAxis tickValues={[-100]} />
                                <VictoryScatter
                                    data={
                                        astronomicalBodyData
                                        .map(
                                            obj => {
                                                let curMeanAnomaly = obj.meanAnomalyAtEpoch + (dateToCheck === undefined ? (daysSinceJ2000 || 0) : dateToCheck) * obj.sweepRate;
                                                return {
                                                    x: obj.distance * cos(unit(curMeanAnomaly, "deg")),
                                                    y: obj.distance * sin(unit(curMeanAnomaly, "deg")),
                                                    distance: obj.distance,
                                                    label: obj.name,
                                                    fill: obj.color,
                                                    size: obj.size
                                                }
                                            }
                                        )
                                    }
                                    labelComponent={
                                        <VictoryTooltip
                                            cornerRadius={2}
                                            constrainToVisibleArea={true}
                                            flyoutPadding={2}
                                            flyoutStyle={{strokeWidth: 0.5, stroke: "#0A090E"}}
                                            pointerWidth={2}
                                            centerOffset={{x: 5}}
                                            orientation={"top"}
                                            dx={({datum}) => datum.size*(sqrt(0.25) as number)}
                                            dy={({datum}) => -datum.size*(sqrt(0.75) as number)}
                                        />
                                    }
                                    style={{
                                        data: { fill: eachFill },
                                        labels: { fontSize: 6, fontFamily: "Atkinson Hyperlegible" }
                                    }}
                                />
                            </VictoryChart>
                        </div>
                    </>
                    : <div className="spinner-container">
                        <RotateLoader size="1.5rem"  color="#FFF8E7" />
                    </div>
                }
            </HeadingLevel>
            <div className="bottom-corner">
                <button onClick={() => {setSliders(!slidersVisible);}}>toggle sliders</button>
                <button onClick={() => window.location.reload()}>reset</button>
                website&nbsp;made&nbsp;by&nbsp;<a title="My linktr.ee" target="_blank" rel="noreferrer" href="https://linktr.ee/noahkuhn">noah&nbsp;kuhn</a>
                <br />
                in ~12 hours just for fun
            </div>
        </>
    );
}